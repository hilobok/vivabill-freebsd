#!/usr/local/bin/php
<?php
 function task_executant($id) {

  include ("/usr/local/etc/vivabill/config.php");
  include ("/usr/local/etc/vivabill/fn_core.php");

  $ddrt=date('Y.m.d H:i:00', time());
  $ddr=date('Y-m-d', time());
  $ddrtm=date('H:i:00', time());

  $func_role = basename(__FILE__)." ".__FUNCTION__ ;
  error_reporting(0);

  function set_task($vfile_task, $vid, $vstatus, $vexport) {
     file_put_contents($vfile_task, "{\"process\":{".
                                    "\"id\":\"".$vid."\",".
                                    "\"pid\":\"".getmypid ()."\",".
                                    "\"status\":\"".$vstatus."\",".
                                    "\"date\":\"".date('Y-m-d H:i:s', time())."\",".
                                    "\"export\":\"".$vexport."\"}}"
    );
  }

  $export="";
  if (!is_dir($gpon_stat_dir.$ddr) && strlen($gpon_stat_dir.$ddr)>0) { mkdir($gpon_stat_dir.$ddr, 0777,true); }

  $file_task=$gpon_dir."gpon_fs_".$id.".task";
  $file_data=$gpon_dir."gpon_fs_".$id.".data";

  set_task ($file_task, $id, "run","");

  $a_data = file_get_contents ($file_data);
  $data = json_decode($a_data, true);
  $ip=$data["ip"];
  $community=$data["community"];
  $snmp_model=$data["snmp_model"];
  $onu_data=$data["onu_data"];
  $session = new SNMP(SNMP::VERSION_2c, $ip, $community);
  $session->valueretrieval = SNMP_VALUE_PLAIN;
  $err = $session->getError();

  if ( $err == "" ) {

   foreach ($onu_data as $row) {

    $srow=explode("-",$row);
    $volt=$srow[0];
    $vonu=$srow[1];
    $vid_comx=$srow[2];

    $gpon_dir=$gpon_stat_dir.$ddr."/".$vid_comx;
    if (!is_dir($gpon_dir) && strlen($gpon_dir)>0) { mkdir($gpon_dir, 0777,true); }

    #$lsvt_stat = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.51.1.5.".$volt.".".$vonu);
    $dsrx_stat = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.51.1.4.".$volt.".".$vonu);
    $ustx_stat = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.51.1.3.".$volt.".".$vonu);
    $usrx_stat = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.51.1.6.".$volt.".".$vonu);
    #$bpw_stat = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.51.1.2.".$volt.".".$vonu);
    #$tmpr_stat = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.51.1.1.".$volt.".".$vonu);

    # DS RX------------
    $file_dsrx = $gpon_dir."/dsrx.log";
    $str_dsrx  = $ddrtm." ".$dsrx_stat.chr(10);
    file_put_contents($file_dsrx, $str_dsrx, FILE_APPEND | LOCK_EX);
    #echo message_addlog($func_role, $file_dsrx." ".$str_dsrx);

    # US TX------------
    $file_ustx = $gpon_dir."/ustx.log";
    $str_ustx  = $ddrtm." ".$ustx_stat.chr(10);
    file_put_contents($file_ustx, $str_ustx, FILE_APPEND | LOCK_EX);
    #echo message_addlog($func_role, $file_ustx." ".$str_ustx);

    # US RX------------
    $file_usrx = $gpon_dir."/usrx.log";
    $str_usrx  = $ddrtm." ".$usrx_stat.chr(10);
    file_put_contents($file_usrx, $str_usrx, FILE_APPEND | LOCK_EX);
    #echo message_addlog($func_role, $file_usrx." ".$str_usrx);

   }

   $session->close();

  }

  set_task ($file_task, $id, "success",$export);


 }
 task_executant($argv[1]);
?>