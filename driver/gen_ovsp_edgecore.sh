#!/usr/local/bin/php
<?php

 function task_executant($id) {

 include ("/usr/local/etc/vivabill/config.php");
 include ("/usr/local/etc/vivabill/fn_core.php");
 $func_role = basename(__FILE__)." ".__FUNCTION__ ;
 error_reporting(0);

 function set_task($vfile_task, $vid, $vstatus, $vcomment) {

   file_put_contents($vfile_task, "{\"process\":{".
                                              "\"id\":\"".$vid."\",".
                                              "\"pid\":\"".getmypid ()."\",".
                                              "\"status\":\"".$vstatus."\",".
                                              "\"date\":\"".date('Y-m-d H:i:s', time())."\",".
                                              "\"comment\":\"".$vcomment."\"}}"
                                              );

 }

 $result="not given";

  $file_task=$ovsp_dir.$id.".task";
  $file_data=$ovsp_dir.$id.".data";

  set_task ($file_task, $id, "run","");

  $data = file_get_contents ($file_data);
  $config = json_decode($data, true);

  $ip=$config["ip"];
  $model=$config["model"];
  $port=$config["port"];
  $speed_new=$config["speed"];
  $snmp_community=$config["snmp"]["community"];
  $snmp_mib=$config["snmp"]["mib"];
  $snmp_fault=$config["snmp"]["fault"];
#  $snmp_savemib=$config["snmp"]["savemib"];
#  $snmp_saveset=$config["snmp"]["saveset"];

  $speed_min=$speed_new-$snmp_fault;
  $speed_max=$speed_new+$snmp_fault;

#  $session = new SNMP(SNMP::VERSION_2c, $ip, $snmp_community, 100000, 2);
  $session = new SNMP(SNMP::VERSION_2c, $ip, $snmp_community);
  $session->valueretrieval = SNMP_VALUE_PLAIN;
  $speed_get = $session->set($snmp_mib.$port,"i", $speed_new );
  $err = $session->getError();

  if ( $err == "" ) {

   if ( $speed_max>=$speed_get && $speed_get>=$speed_min ) {

    $comment =  $model." ".$ip." ".$snmp_mib.$port."=".$speed_new;
    $src_conf = $session->set(".1.3.6.1.4.1.259.10.1.42.101.1.24.1.1.0", "i", "2");

    if ( $src_conf == "2" ) {

     $dst_conf = $session->set(".1.3.6.1.4.1.259.10.1.42.101.1.24.1.3.0","i","3");

     if ( $dst_conf == "3" ) {

      $pcopy = $session->set(".1.3.6.1.4.1.259.10.1.42.101.1.24.1.8.0","i","2");

      if ( $pcopy == "1" ) {
       $result="success";
      } else {
       $err = $model." ".$ip." did not save pcopy='".$pcopy."'. Must be '1'";
      }

     } else {
      $err = $model." ".$ip." did not save dst_conf='".$dst_conf."'. Must be '3'";
     }

    } else {
     $err = $model." ".$ip." did not save src_conf='".$src_conf."'. Must be '2'";
    }

   } else {
    $err =  $model." ".$ip." ".$snmp_mib.$port."='" .$speed_get."'. Given '".$speed_new."'";
   }

  } else {
   echo system_addlog($func_role, "1", $err);
  }

  $session->close();

  if  ( $result == "success") {
   set_task ($file_task, $id, "success",$comment);
  }  else {
   set_task ($file_task, $id, "error", $err);
  }

 }

 task_executant($argv[1]);

?>