#!/usr/local/bin/php
<?php
 function task_executant($id) {

  include ("/usr/local/etc/vivabill/config.php");
  include ("/usr/local/etc/vivabill/fn_core.php");

  $ddrt=date('Y.m.d H:i:00', time());
  $ddr=date('Y-m-d', time());
  $ddrtm=date('H:i:00', time());

  $func_role = basename(__FILE__)." ".__FUNCTION__ ;
  error_reporting(0);

  function set_task($vfile_task, $vid, $vstatus, $vexport) {
     file_put_contents($vfile_task, "{\"process\":{".
                                    "\"id\":\"".$vid."\",".
                                    "\"pid\":\"".getmypid ()."\",".
                                    "\"status\":\"".$vstatus."\",".
                                    "\"date\":\"".date('Y-m-d H:i:s', time())."\",".
                                    "\"export\":\"".$vexport."\"}}"
    );
  }

  $export="";

  try {

  $file_task=$gpon_dir."gpon_bi_".$id.".task";
  $file_data=$gpon_dir."gpon_bi_".$id.".data";

  set_task ($file_task, $id, "run","");

  $array_snmp_model = array("HG8245C","HG8245D");

  $a_data = file_get_contents ($file_data);
  $data = json_decode($a_data, true);
  $ip=$data["ip"];
  $community=$data["community"];
  $snmp_model=$data["snmp_model"];
  $session = new SNMP(SNMP::VERSION_2c, $ip, $community);
  $session->valueretrieval = SNMP_VALUE_PLAIN;
  $err = $session->getError();

  if ( $err == "" ) {

   $session->valueretrieval = SNMP_VALUE_PLAIN;
   $r_ifname = $session->walk(".1.3.6.1.2.1.31.1.1.1.1");

   foreach ( $r_ifname as $ifid => $ifname ) {
    $ifid = explode ("IF-MIB::ifName.", $ifid);
    $olt_idport = $ifid[1];

    if ( preg_match('#' . "GPON" . '#i', $ifname) ) {

     $ifname = str_replace(" ","", $ifname);
     $ifname = str_replace("GPON","", $ifname);
     $ifchassis = explode ("/", $ifname);

     $chassis = $ifchassis[0];
     $slot = $ifchassis[1];
     $port = $ifchassis[2];

#     echo message_addlog($func_role,$chassis."-".$slot."-".$port);

     // START STATUS PORT ---------------------------------------------
     $val_ud="x";
     $val_os="x";
     $val_sp="x";
     $val_pu="x";
     $val_error_value_up="x";
     $val_error_value_dn="x";

     $arr_ud = shell_exec("/usr/local/bin/snmpget -v 2c -c ".$community." -Ir -t 2 -OqvEt ".$ip." .1.3.6.1.2.1.2.2.1.7.".$olt_idport);
     $arr_ud = explode(chr(10),$arr_ud);
     $val_ud = $arr_ud[0];

     // status UP -----------------------------------------------------
     if ( $val_ud == "1" ) {

      $arr_os = shell_exec("/usr/local/bin/snmpget -v 2c -c ".$community." -Ir -t 2 -OqvEt ".$ip." .1.3.6.1.2.1.2.2.1.8.".$olt_idport);
      $arr_os = explode(chr(10),$arr_os);
      $val_os = $arr_os[0];

      if ( $val_os == "1" ) {

       $arr_sp = shell_exec("/usr/local/bin/snmpget -v 2c -c ".$community." -Ir -t 2 -OqvEt ".$ip." .1.3.6.1.2.1.2.2.1.5.".$olt_idport);
       $arr_sp = explode(chr(10),$arr_sp);
       $val_sp = $arr_sp[0];


       if ( $val_sp > 0) {
        if ($val_sp == "10") { $val_sp="10Mb"; }
        if ($val_sp == "100") { $val_sp="100Mb"; }
        if ($val_sp == "1000") { $val_sp="1Gb"; }
        if ($val_sp == "2488320000") { $val_sp="2.5Gb"; }
        if ($val_sp == "12488320000") { $val_sp="10Gb"; }
        if ($val_sp == "32488320000") { $val_sp="40Gb"; }
        if ($val_sp == "52488320000") { $val_sp="100Gb"; }
       } else {
        $val_sp="auto";
       }

       $arr_pu = shell_exec("/usr/local/bin/snmpget -v 2c -c ".$community." -Ir -t 2 -OqvEt ".$ip." .1.3.6.1.2.1.2.2.1.9.".$olt_idport);
       $arr_pu = explode(chr(10),$arr_pu);
       $val_pu = $arr_pu[0];

       $val_status=$val_sp;

      }

      // status DOWN -----------------------------
      if ( $val_os == "2" ) {
       $val_status='down';
      }

      // status TESTING --------------------------
      if ( $val_os == "3" ) {
       $val_status='testing';
      }

      // status UNKNOWN --------------------------
      if ( $val_os == "4" ) {
       $val_status='unknown';
      }

      // status DORMANT --------------------------
      if ( $val_os == "5" ) {
       $val_status='dormant';
      }

      // status NotPresent -----------------------
      if ( $val_os == "6" ) {
       $val_status='notpresent';
      }

      // status LLD ------------------------------
      if ( $val_os == "7" ) {
       $val_status='lowerlayerdown';
      }

     }

     // status CLOSE -------------------------------------------------------
     if ( $val_ud == "2" ) {
      $val_status='close';
     }

     // status TEST --------------------------------------------------------
     if ( $val_ud == "3" ) {
      $val_status='test';
     }

     $arr_error_value_up = shell_exec("/usr/local/bin/snmpget -v 2c -c ".$community." -Ir -t 2 -OqvEt ".$ip." .1.3.6.1.2.1.2.2.1.14.".$olt_idport);
     $arr_error_value_up = explode(chr(10),$arr_error_value_up);
     $val_error_value_up = $arr_error_value_up[0];

     $arr_error_value_dn = shell_exec("/usr/local/bin/snmpget -v 2c -c ".$community." -Ir -t 2 -OqvEt ".$ip." .1.3.6.1.2.1.2.2.1.20.".$olt_idport);
     $arr_error_value_dn = explode(chr(10),$arr_error_value_dn);
     $val_error_value_dn = $arr_error_value_dn[0];

     if ( $val_error_value_up == "4294967295" ) { $val_error_value_up="-"; }
     if ( $val_error_value_dn == "4294967295" ) { $val_error_value_dn="-"; }

     $val_error_value = $val_error_value_up."/".$val_error_value_dn;

     // STOP STATUS PORT ---------------------------------------------------

     $session->valueretrieval = SNMP_VALUE_PLAIN;

     $hwBoardInOcts  = $session->get(".1.3.6.1.4.1.2011.6.48.2.1.1.".$chassis.".".$slot);
     $hwBoardOutOcts = $session->get(".1.3.6.1.4.1.2011.6.48.2.1.2.".$chassis.".".$slot);
     $hwMusaBoardCpuRate = $session->get(".1.3.6.1.4.1.2011.2.6.7.1.1.2.1.5.".$chassis.".".$slot);
     $hwMusaBoardRamUseRate = $session->get(".1.3.6.1.4.1.2011.2.6.7.1.1.2.1.6.".$chassis.".".$slot);
     $hwMusaBoardSlotDesc = $session->get("1.3.6.1.4.1.2011.2.6.7.1.1.2.1.7.".$chassis.".".$slot);

     $hwMusaBoardTemperature = $session->get(".1.3.6.1.4.1.2011.2.6.7.1.1.2.1.10.".$chassis.".".$slot);
     $hwMusaBoardSN = $session->get(".1.3.6.1.4.1.2011.6.3.3.2.1.11.".$chassis.".".$slot);
     $val_uptime = $session->get(".1.3.6.1.2.1.1.3.0");

     $max_count_onu="64";

     if ( $hwMusaBoardSlotDesc == "H805GPBD" ) { $max_count_onu="128"; }
     if ( $hwMusaBoardSlotDesc == "H802GPBD" ) { $max_count_onu="128"; }

     $hwGponDeviceOltControlRogueOntExt = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.21.1.12.".$olt_idport);
     $hwGponDeviceOltControlOpticModuleStatus = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.21.1.13.".$olt_idport);

     if ( $hwGponDeviceOltControlOpticModuleStatus == '1') {
      $hwGponOltOpticsModuleInfoWaveLength = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.22.1.15.".$olt_idport);
      $hwGponOltOpticsDdmInfoTemperature = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.23.1.1.".$olt_idport);
      $hwGponDeviceOltControlDnFecEnabled = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.21.1.3.".$olt_idport);
      $hwGponDeviceOltControlAutofindOntEnable = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.21.1.4.".$olt_idport);
      $hwGponDeviceOltControlOntNum  = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.21.1.16.".$olt_idport);
      $hwGponOltOpticsModuleInfoConnector = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.22.1.3.".$olt_idport);
      $hwGponOltOpticsModuleInfoVendorName = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.22.1.11.".$olt_idport);
      $hwGponOltOpticsModuleInfoVendorPN = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.22.1.13.".$olt_idport);
      $dstx_stat = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.23.1.4.".$olt_idport);
      $hwGponOltOpticsModuleInfoVendorSN  = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.22.1.20.".$olt_idport);
     } else {
      $hwGponOltOpticsModuleInfoWaveLength = "";
      $hwGponOltOpticsDdmInfoTemperature = "";
      $hwGponDeviceOltControlDnFecEnabled = "";
      $hwGponDeviceOltControlAutofindOntEnable = "";
      $hwGponDeviceOltControlOntNum  = "";
      $hwGponOltOpticsModuleInfoConnector = "";
      $hwGponOltOpticsModuleInfoVendorName = "";
      $hwGponOltOpticsModuleInfoVendorPN = "";
      $dstx_stat = "";
      $hwGponOltOpticsModuleInfoVendorSN  = "";
     }

     $export=$export."P~s~".
     $olt_idport."~s~".
     $chassis."~s~".
     $slot."~s~".
     $port."~s~".

     $hwBoardInOcts."~s~".
     $hwBoardOutOcts."~s~".
     $hwMusaBoardCpuRate."~s~".
     $hwMusaBoardRamUseRate."~s~".
     $hwMusaBoardSlotDesc."~s~".

     $hwGponOltOpticsModuleInfoWaveLength."~s~".
     $hwGponOltOpticsDdmInfoTemperature."~s~".
     $hwGponDeviceOltControlDnFecEnabled."~s~".
     $hwGponDeviceOltControlAutofindOntEnable."~s~".
     $hwGponDeviceOltControlRogueOntExt."~s~".
     $hwGponDeviceOltControlOntNum."~s~".
     $hwGponOltOpticsModuleInfoConnector."~s~".
     trim($hwGponOltOpticsModuleInfoVendorName," ")."~s~".
     trim($hwGponOltOpticsModuleInfoVendorPN," ")."~s~".

     $dstx_stat."~s~".
     trim($hwGponOltOpticsModuleInfoVendorSN," ")."~s~".
     $hwMusaBoardTemperature."~s~".
     $hwMusaBoardSN."~s~".
     $max_count_onu."~s~".
     $hwGponDeviceOltControlOpticModuleStatus."~s~".
     $val_status."~s~".
     ticks_to_time($val_pu,$val_uptime)."~s~".
     $val_error_value."~ent~";


     // FOR ONU STAT // --------------------------------------------------------------

     $session->valueretrieval = SNMP_VALUE_LIBRARY;
     $r_onu = $session->walk(".1.3.6.1.4.1.2011.6.128.1.1.2.43.1.3."."$olt_idport");

     foreach ($r_onu as $id_ronu => $val_ronu) {
      $id_ronu = explode ('.', $id_ronu);
      $onu_idport=$id_ronu[11];

      $serial_stat = explode ('Hex-STRING: ', $val_ronu);
      $serial_stat = str_replace (' ','',$serial_stat[1]);

      $session->valueretrieval = SNMP_VALUE_PLAIN;
      $run_stat = $session->get("1.3.6.1.4.1.2011.6.128.1.1.2.46.1.15.".$olt_idport.".".$onu_idport);

      if ( $run_stat == "1" ) {

       $describe_stat = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.43.1.9.".$olt_idport.".".$onu_idport);

       $hw_stat = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.45.1.1.".$olt_idport.".".$onu_idport);
       $uptime_stat = $session->get(".1.3.6.1.4.1.2011.6.145.1.1.1.4.1.1.".$olt_idport.".".$onu_idport);
       $regtime_stat = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.101.1.6.".$olt_idport.".".$onu_idport.".0");
       $lsvt_stat = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.51.1.5.".$olt_idport.".".$onu_idport);
       $dsrx_stat = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.51.1.4.".$olt_idport.".".$onu_idport);
       $ustx_stat = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.51.1.3.".$olt_idport.".".$onu_idport);
       $usrx_stat = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.51.1.6.".$olt_idport.".".$onu_idport);

       #$dstx_stat = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.51.1.6.".$olt_idport.".".$onu_idport);
       $bpw_stat = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.51.1.2.".$olt_idport.".".$onu_idport);
       $tmpr_stat = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.51.1.1.".$olt_idport.".".$onu_idport);
       $dist_stat = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.46.1.20.".$olt_idport.".".$onu_idport);
       $fw_stat = $session->get(".1.3.6.1.4.1.2011.6.128.1.1.2.45.1.5.".$olt_idport.".".$onu_idport);

       $export=$export."R~s~".
       $olt_idport."~s~".
       $onu_idport."~s~".
       $serial_stat."~s~".
       $describe_stat."~s~".
       $hw_stat."~s~".
       $uptime_stat."~s~".
       $regtime_stat."~s~".
       $lsvt_stat."~s~".
       $dsrx_stat."~s~".
       $ustx_stat."~s~".
       $usrx_stat."~s~".
       $dstx_stat."~s~".
       $bpw_stat."~s~".
       $tmpr_stat."~s~".
       $dist_stat."~s~".
       $fw_stat."~ent~";

      }

     }

    }


   }

   $session->close();

  }

  set_task ($file_task, $id, "success",$export);

  } catch (Exception $e) {
   set_task ($file_task, $id, "error",$e->getMessage());
   echo system_addlog($func_role,"1", $e->getMessage());
  }

 }
 task_executant($argv[1]);
?>