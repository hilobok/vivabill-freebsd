<?php

function alarm_action ($id, $ch, $connect_url, $connect_api, $action, $export) {
 include ("config.php");
 $func_role = basename(__FILE__)." ".__FUNCTION__ ;
 $result="";
 curl_setopt($ch, CURLOPT_URL, $connect_url."set_alarm");

 $pdata = array(
  'id' => $id,
  'action' => $action,
  'export' => $export,
  'api' => $connect_api
 );

 curl_setopt($ch, CURLOPT_POSTFIELDS, stripslashes(json_encode($pdata)));

 $return=curl_exec($ch);

 if(curl_errno($ch)) {
  echo  system_addlog($func_role,"1","Curl error: ". curl_error($ch) );
 } else {

  $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  if ($httpcode == "200") {
   $result_json = json_decode($return, true);
   $result = $result_json["result"];
  } else {
   $last_url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
   echo system_addlog ($func_role,"1","Error [".$httpcode."] ".$last_url);
  }

 }
 return $result;
}

function alarm_task ($id_comx_manager, $action, $ch, $connect_url, $connect_api) {
 include ("config.php");
 $func_role = basename(__FILE__)." ".__FUNCTION__ ;

 $data_file=$alarm_dir.$action."_".$id_comx_manager.".data";
 $task_file=$alarm_dir.$action."_".$id_comx_manager.".task";

 if (!file_exists($task_file)) {

  curl_setopt($ch, CURLOPT_URL, $connect_url."get_alarm_task");

  $pdata = array(
   'id_comx_manager' => $id_comx_manager,
   'action' => $action,
   'api' => $connect_api
  );

  curl_setopt($ch, CURLOPT_POSTFIELDS, stripslashes(json_encode($pdata)));

  $return=curl_exec($ch);

  if(curl_errno($ch)) {
   echo  system_addlog($func_role,"1","Curl error: ". curl_error($ch) );
  } else {
   $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
   if ($httpcode == "200") {

    $config_data = json_decode($return,true);
    $fp = fopen($data_file, "w");
    fwrite($fp, json_encode($config_data));
    fclose($fp);

    $execute_name=$driver_dir."gen_".$action.".sh";
    $execute_command=$execute_name." ".$id_comx_manager." < /dev/null >> ".$log_dir."vivabill.log &";
    $return=system_exec($execute_command);

    if ( $return != "0" ) {
     echo system_addlog($func_role, $return, $execute_command);
    }

   } else {
    $last_url=curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    echo system_addlog($func_role,"1","Error [".$httpcode."] ".$last_url);
   }

  }

 } else {
  echo message_addlog($func_role,"Task ".$action."_".$id_comx_manager." is exists");
 }

}


function alarm_check ($action, $ch, $connect_url, $connect_api) {
 include("config.php");
 $func_role = basename(__FILE__)." ".__FUNCTION__ ;

 foreach (glob($alarm_dir.$action."*.task") as $filename) {
  $data = file_get_contents ($filename);
  $config_json = json_decode($data, true);

  $id=$config_json["process"]["id"];
  $pid=$config_json["process"]["pid"];
  $status=$config_json["process"]["status"];

  $file_task=$alarm_dir.$action."_".$id.".task";
  $file_data=$alarm_dir.$action."_".$id.".data";

  if ( $status == "run") {

  }

  if ( $status == "success") {

   $export=$config_json["process"]["export"];
   $arr_export=explode("~ent~",$export);
   $cou_export = count($arr_export) - 1;
   if ( alarm_action($id, $ch, $connect_url, $connect_api, $action, $export) == "success") {
    if ( $cou_export > 0 ) {
     echo system_addlog($func_role, "0", "Task ".$action." ID-".$id." - changed ".$cou_export." rows");
    }
   } else {
    echo system_addlog($func_role, "1", "Task ".$action." ID-".$id." - error");
   }

   if (file_exists($file_task)) { unlink($file_task); }
   if (file_exists($file_data)) { unlink($file_data); }

  }

  if ( $status == "error") {

   echo system_addlog($func_role, "1", "Task ".$action." ID-".$id." - error");
   if (file_exists($file_task)) { unlink($file_task); }
   if (file_exists($file_data)) { unlink($file_data); }

  }

 }

}

?>
