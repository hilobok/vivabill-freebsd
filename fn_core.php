<?php

function get_network($ip, $cidr) {
 $bitmask = $cidr == 0 ? 0 : 0xffffffff << (32 - $cidr);
 return long2ip(ip2long($ip) & $bitmask);
}

function addlog ($role, $message) {
 include("config.php");
 $W="\033[0;37m";
 $N="\033[0m";
 $result="[MESSAGE] ";
 $resultc="${W}[MESSAGE]${N} ";
 $role="[".$role."] ";
 file_put_contents($log_file, "[".date('Y-m-d H:i:s', time())."] ".$result.$role.$message.chr(10), FILE_APPEND | LOCK_EX);
 return "[".date('Y-m-d H:i:s', time())."] ".$resultc.$role.$message.chr(10);
}

function system_addlog ($role, $result, $command) {
 include("config.php");
 $iresult=$result;
 $R="\033[0;31m";
 $G="\033[0;32m";
 $N="\033[0m";
 if ($result=="0") {
  $result ="[SUCCESS] ";
  $resultc="${G}[SUCCESS]${N} ";
 } else {
  $result ="[ERROR ".$iresult."] ";
  $resultc="${R}[ERROR ".$iresult."]${N} ";

  $data = file_get_contents ($vivapi);
  $config = json_decode($data, true);
  $uuid=$config["uuid"];
  file_put_contents($errors_dir."errors.tmp",$uuid."	".date('Y-m-d H:i:s',time())."	".$role."	".$command.chr(10), FILE_APPEND | LOCK_EX);

 }
 $role="[".$role."] ";
 file_put_contents($log_file, "[".date('Y-m-d H:i:s', time())."] ".$result.$role.$command.chr(10), FILE_APPEND | LOCK_EX);
 return "[".date('Y-m-d H:i:s', time())."] ".$resultc.$role.$command.chr(10);
}

function message_addlog ($role, $command) {
 include("config.php");
 $W="\033[0;37m";
 $N="\033[0m";
 $result="[MESSAGE] ";
 $resultc="${W}[MESSAGE]${N} ";
 $role="[".$role."] ";
 file_put_contents($log_file, "[".date('Y-m-d H:i:s', time())."] ".$result.$role.$command.chr(10), FILE_APPEND | LOCK_EX);
 return "[".date('Y-m-d H:i:s', time())."] ".$resultc.$role.$command.chr(10);
}

function system_exec_addlog ($role, $command) {
 include("config.php");
 $output=system ($command,$result);
 $iresult=$result;
 $R="\033[0;31m";
 $G="\033[0;32m";
 $N="\033[0m";
 if ($result=="0") {
  $result="[SUCCESS] ";
  $resultc="${G}[SUCCESS]${N} ";
 } else {
  $result="[ERROR ".$iresult."] ";
  $resultc="${R}[ERROR ".$iresult."]${N} ";

  $data = file_get_contents ($vivapi);
  $config = json_decode($data, true);
  $uuid=$config["uuid"];
  file_put_contents($errors_dir."errors.tmp",$uuid."	".date('Y-m-d H:i:s',time())."	".$role."	".$command.chr(10), FILE_APPEND | LOCK_EX);

 }
 $role="[".$role."] ";
 file_put_contents($log_file, "[".date('Y-m-d H:i:s', time())."] ".$result.$role.$command.chr(10), FILE_APPEND | LOCK_EX);
 return "[".date('Y-m-d H:i:s', time())."] ".$resultc.$role.$command.chr(10);
}

function system_exec_addlog_puppet ($role, $command) {
 include("config.php");
 $output=system ($command,$result);
 $iresult=$result;
 $R="\033[0;31m";
 $G="\033[0;32m";
 $N="\033[0m";
 if ( ($result=="0") || ($result=="2") ) {
  $result="[SUCCESS] ";
  $resultc="${G}[SUCCESS]${N} ";
 } else {
  $result="[ERROR ".$iresult."] ";
  $resultc="${R}[ERROR ".$iresult."]${N} ";

  $data = file_get_contents ($vivapi);
  $config = json_decode($data, true);
  $uuid=$config["uuid"];
  file_put_contents($errors_dir."errors.tmp",$uuid."	".date('Y-m-d H:i:s',time())."	".$role."	".$command.chr(10), FILE_APPEND | LOCK_EX);

 }
 $role="[".$role."] ";

 file_put_contents($log_file, "[".date('Y-m-d H:i:s', time())."] ".$result.$role.$command.chr(10), FILE_APPEND | LOCK_EX);
 return "[".date('Y-m-d H:i:s', time())."] ".$resultc.$role.$command.chr(10);
}

function system_exec ($command) {
 $output=system ($command, $result);
 return $result;
}

function identical_values( $arrayA , $arrayB ) {
 sort( $arrayA );
 sort( $arrayB );
 return $arrayA == $arrayB;
}

function get_device_online ($vip, $vmode, $vsocket, $vmib, $vcommunity) {
 include("config.php");
 $return="0";

 // by socket
 if ( $vmode == "3" ) {
  $filep = fsockopen ($vip, $vsocket, $errno, $errstr, 1);
  if ($filep) { $return="1"; }
 }

 // by snmp
 if ( $vmode == "2" ) {
  $session = new SNMP(SNMP::VERSION_2c, $vip, $vcommunity, 100000, 2);
  $session->valueretrieval = SNMP_VALUE_PLAIN;
  $val_descr = $session->get($vmib);
  $err = $session->getError();
  $session->close();
  if ( $err == "" ) { $return="1"; }
 }

 // by ping
 if ( $vmode == "1" ) {
  exec($ping." -t 1 -c 2 ".$vip, $output, $retval);
  if ($retval == 0) { $return="1"; }
 }

 return $return;

}


function get_exists_vi_cron($vi_cron,$period) {
 if ( !isset($vi_cron[$period])) {
  return false;
 } else {
  return true;
 }
}

function seconds2human($ss) {
 $s = $ss%60;
 $m = floor(($ss%3600)/60);
 $h = floor(($ss%86400)/3600);
 $d = floor(($ss%2592000)/86400);
 $M = floor($ss/2592000);
 return str_pad($M,1,0, STR_PAD_LEFT)."m ".
        str_pad($d,2,0, STR_PAD_LEFT)."d ".
        str_pad($h,2,0, STR_PAD_LEFT).":".
        str_pad($m,2,0, STR_PAD_LEFT)."";
        "${M}m,${d}d,$h:$m:$s";
}

function ticks_to_time($ticks,$systime) {
 if ( $ticks == "x" ) {
  return "";
 } else {
  return (seconds2human(($systime-$ticks)/100));
 }
}

function snmptrap_parser($snmpdata) {

 $rdata=explode(chr(10),$snmpdata);
 $cou_rdata=count($rdata);
 $date = date('Y-m-d H:i:s', time());

 $export=json_encode(array(
  "head" => "unknown",
  "date" => $date,
  "acount" => $cou_data,
  "snmpdata" => $snmpdata
 ));

 // find UP/DOWN port
 if ( $cou_rdata == 8 ) {

  $ip="";
  $port="";
  $status="";

  foreach ( $rdata as $row ) {

   list($header,$ip1) = split("[][]+", $row);
   if ( $ip1 != "" ) {
    $ip=$ip1;
   }

   list ($mib1, $mib2, $mib3,
         $mib4, $mib5, $mib6,
         $mib7, $mib8, $mib9,
         $mib10, $mib11, $mib12 ) = split("[\. ]", $row);
   if ( $mib11 != "" ) {

    if ( $mib11 == "1" && $mib12 != "0" ) {
     $rparam=explode(" ",$row);
     $port=$rparam[1];
    }

    if ( $mib11 == "8" && $mib12 != "0" ) {
     $rparam=explode(" ",$row);
     $status=$rparam[1];
    }

   }

  }

  $export=json_encode(array(
   "head" => "alarm",
   "date" => $date,
   "ip" => $ip ,
   "port" => $port,
   "status" => $status
  ));

 }

 return $export;

}

function check_vivabill_tasks ($uuid, $ch, $connect_url, $connect_api) {
 include ("config.php");
 $func_role = basename(__FILE__)." ".__FUNCTION__ ;

 $pjson =array();

 foreach (glob(
     "{".
      $alarm_dir."*.task,".
      $cmts_dir."*.task,".
      $gpon_dir."*.task,".
      $stat_dir."*.task".
     "}"
    ,GLOB_BRACE) as $filename) {
    $data = file_get_contents ($filename);
    $config_json = json_decode($data, true);

    $status = $config_json["process"]["status"];

    if ( $status =='error' ) {
     $error = $config_json["process"]["export"];
    } else {
     $error = '';
    }

    $info = pathinfo($filename);

    array_push($pjson,  array(
     'id' => $config_json["process"]["id"],
     'pid' => $config_json["process"]["pid"],
     'name' => basename($filename,'.'.$info['extension']),
     'path' => $filename,
     'status' => $status,
     'date' => $config_json["process"]["date"],
     'error' => $error
    ));

    }

    curl_setopt($ch, CURLOPT_URL, $connect_url."set_vivabill_tasks");

     $pdata = array(
      'uuid' => $uuid,
      'data' => $pjson,
      'api' => $connect_api
     );

  curl_setopt($ch, CURLOPT_POSTFIELDS, stripslashes(json_encode($pdata, JSON_UNESCAPED_UNICODE)));
  $return=curl_exec($ch);

  if(curl_errno($ch)) {
   echo  system_addlog($func_role,"1","Curl error: ". curl_error($ch) );
  } else {

   $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
   if ($httpcode == "200") {
    $result_json = json_decode($return, true);
    $result = $result_json["result"];
   } else {
    $last_url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    echo system_addlog ($func_role,"1","Error [".$httpcode."] ".$last_url);
   }

  }

}

?>