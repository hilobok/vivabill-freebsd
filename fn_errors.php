<?php

function get_errors ($connect_upload, $connect_username, $connect_password) {
 include ("config.php");
 $func_role = basename(__FILE__)." ".__FUNCTION__ ;

 if (file_exists($errors_dir."errors.tmp")) {
  rename($errors_dir."errors.tmp", $errors_dir.uniqid().".errors");
 }

 foreach (glob( $errors_dir."*.errors") as $errors_file_full) {

  $errors_file=basename ($errors_file_full);

  if (!function_exists('curl_file_create')) {
   function curl_file_create($filename, $mimetype = '', $postname = '') {
   return "@$filename;filename="
   . ($postname ?: basename($filename))
   . ($mimetype ? ";type=$mimetype" : '');
   }
  }

   $cfile = curl_file_create($errors_dir.$errors_file,'text/plain',$errors_file);
   $tdata = array('files' => $cfile);

   $chu = curl_init();

   curl_setopt($chu, CURLOPT_SAFE_UPLOAD, true);
   curl_setopt($chu, CURLOPT_SSL_VERIFYPEER, 1);
   curl_setopt($chu, CURLOPT_CAINFO, $certs_dir."viva-cert.pem"); 
   curl_setopt($chu, CURLOPT_CONNECTTIMEOUT, 30);
   curl_setopt($chu, CURLOPT_HEADER, 0);
   curl_setopt($chu, CURLOPT_USERPWD, $connect_username . ":" . $connect_password);
   curl_setopt($chu, CURLOPT_POST, 1);
   curl_setopt($chu, CURLOPT_RETURNTRANSFER, TRUE);
   curl_setopt($chu, CURLOPT_URL, $connect_upload."/upload");
   curl_setopt($chu, CURLOPT_POSTFIELDS, $tdata );
   $return=curl_exec($chu);

   if(curl_errno($chu)) {
    echo  system_addlog($func_role,"1","Curl error: ". curl_error($chu) );
   } else {

    $httpcode = curl_getinfo($chu, CURLINFO_HTTP_CODE);
    if ($httpcode == "200") {

     $r=json_decode($return, true);
     $result=$r["result"];

     if ( $result == "success" ) {
      unlink($errors_dir.$errors_file);
      echo system_addlog ($func_role,"0", "success upload ".$errors_file);
     }

    } else {
     $last_url=curl_getinfo($chu, CURLINFO_EFFECTIVE_URL);
     echo system_addlog($func_role,"1","Error [".$httpcode."] ".$last_url);
    }

   }

   curl_close ($chu);

  }

}

?>